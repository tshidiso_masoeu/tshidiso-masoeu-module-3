import 'package:flutter/material.dart';
import 'package:myapp/Edit_Profile.dart';
import 'package:myapp/Login.dart';
import 'package:myapp/Page1.dart';
import 'package:myapp/Page2.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Dashboard")),
      body: Center(
        child: DashboardPg(),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Container(
          height: 50.0,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const Login()),
          )
        },
        child: const Icon(Icons.home),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

class DashboardPg extends StatelessWidget {
  const DashboardPg({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          const SizedBox(height: 40),
          ElevatedButton(
              child: const Text('COMMING SOON'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Page1()),
                );
              }),
          const SizedBox(height: 40),
          ElevatedButton(
              child: const Text('COMMING SOON'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Page2()),
                );
              }),
          const SizedBox(height: 40),
          ElevatedButton(
              child: const Text('EDIT PROFILE'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Edit_Profile()),
                );
              })
        ],
      ),
    );
  }
}
